import React from 'react';
import { StatusBar, View, StyleSheet, Platform } from 'react-native';
import { COLOR_TAB_BAR_BLUE } from '../../styles/colors';

const STATUS_BAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;

export default (CustomStatusBar = ({
    backgroundColor = COLOR_TAB_BAR_BLUE,
    ...props
}) => (
    <View style={[styles.statusBar, { backgroundColor }]}>
        <StatusBar
            translucent
            barStyle="dark-content"
            backgroundColor={backgroundColor}
            {...props}
        />
    </View>
));

const styles = StyleSheet.create({
    statusBar: {
        height: STATUS_BAR_HEIGHT
    }
});
