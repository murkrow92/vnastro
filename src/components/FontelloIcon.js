import React from 'react';
import { createIconSetFromFontello } from 'react-native-vector-icons';
import fontelloConfig from '../../assets/fonts/config';
const Icon = createIconSetFromFontello(fontelloConfig);

export default (FontelloIcon = ({ name, size, color }) => (
    <Icon name={name} size={size} color={color} />
));
