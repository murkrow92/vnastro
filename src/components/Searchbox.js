import React from 'react';
import { Keyboard, Platform, StyleSheet } from 'react-native';
import {
    COLOR_DARK_33,
    COLOR_GREY_99,
    COLOR_GREY_F7,
    COLOR_WHITE,
} from '../../styles/colors';
import { SearchBar } from 'react-native-elements';

export default class Searchbox extends React.Component {
    render() {
        const { text } = this.props;
        const platform = Platform.OS === 'ios' ? 'ios' : 'default';
        const clearIcon =
            Platform.OS === 'ios'
                ? true
                : text !== ''
                  ? { color: '#86939e', name: 'close' }
                  : { color: 'transparent', name: 'close' };
        return (
            <SearchBar
                platform={platform}
                cancelButtonTitle="Hủy"
                onSubmitEditing={() => {}}
                value={text}
                lightTheme={true}
                autoCapitalize="sentences"
                autoCorrect={false}
                placeholderTextColor={COLOR_GREY_99}
                onChangeText={text => this.props.search(text)}
                onClearText={() => {
                    this.props.search('');
                }}
                placeholder={this.props.placeholder || 'Tìm kiếm'}
                clearIcon={clearIcon}
                containerStyle={
                    Platform.OS === 'ios'
                        ? {
                              borderBottomColor: 'transparent',
                              borderTopColor: 'transparent',
                          }
                        : styles.inputContainer
                }
                inputStyle={Platform.OS === 'ios' ? {} : styles.inputWrapper}
                onCancel={() => {
                    Keyboard.dismiss();
                    this.props.search('');
                }}
            />
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: COLOR_WHITE,
    },
    inputContainer: {
        justifyContent: 'center',
        borderBottomColor: 'transparent',
        borderTopColor: 'transparent',
        backgroundColor: 'transparent',
    },
    inputWrapper: {
        justifyContent: 'center',
        backgroundColor: COLOR_GREY_F7,
        borderRadius: 5,
        fontFamily: 'Arial',
        fontSize: 13,
        color: COLOR_DARK_33,
    },
});
