import React from 'react';
import { BANNER_ID } from '../../config';
import { View } from 'react-native';
import { AdMobBanner } from 'react-native-admob';

export default class AdBanner extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isVisible: false
        };
    }

    render() {
        const { isVisible } = this.state;
        return isVisible ? (
            <View
                style={{
                    alignItems: 'center',
                    justifyContent: 'center'
                }}
            >
                <AdMobBanner
                    adSize="smartBannerPortrait"
                    adUnitID={BANNER_ID}
                    testDevices={[AdMobBanner.simulatorId]}
                    onAdFailedToLoad={() => {
                        this.onError();
                    }}
                />
            </View>
        ) : (
            <View />
        );
    }

    componentDidMount() {
        this.setState({
            isVisible: true
        });
    }

    onError(error) {
        console.log('banner error: ', error);
        this.setState({
            isVisible: false
        });
    }
}
