import { combineReducers } from 'redux';
import {
    ACTION_ON_LOGIN_FACEBOOK_FAILED,
    ACTION_ON_LOGIN_FACEBOOK_SUCCESS,
    ACTION_ON_LOGIN_VNASTRO_FAILED,
    ACTION_ON_LOGIN_VNASTRO_SUCCESS
} from '../actions/LoginActions';

const loginReducer = (state = {}, action) => {
    switch (action.type) {
        case ACTION_ON_LOGIN_FACEBOOK_SUCCESS:
            return {
                ...state,
                loggedInFacebook: true,
                loggingInVnAstro: true,
                accessToken: action.accessToken
            };
        case ACTION_ON_LOGIN_FACEBOOK_FAILED:
            return {
                ...state,
                loggedInFacebook: false,
                loggingInVnAstro: false
            };
        case ACTION_ON_LOGIN_VNASTRO_SUCCESS:
            return {
                ...state,
                ...action.facebook,
                vnastroToken: action.vnastroToken,
                loggingInVnAstro: false,
                loggedInVnAstro: true
            };
        case ACTION_ON_LOGIN_VNASTRO_FAILED:
            return {
                ...state,
                loggingInVnAstro: false,
                loggedInVnAstro: false
            };
        default:
            return state;
    }
};

export default combineReducers({
    data: loginReducer
});
