import { combineReducers } from 'redux';
import {
    FETCH_ASTRO_FAILED,
    FETCH_ASTRO_SUCCESS,
    REFRESH_LIST_ASTRO,
} from '../actions/AstroActions';

const astroReducer = (state = { refreshing: false }, action) => {
    switch (action.type) {
        case FETCH_ASTRO_SUCCESS:
            return {
                ...state,
                ...action.response,
                refreshing: false,
            };
        case FETCH_ASTRO_FAILED:
            return {
                ...state,
                ...action.error,
                refreshing: false,
            };
        case REFRESH_LIST_ASTRO:
            return {
                ...state,
                refreshing: true,
            };
        default:
            return state;
    }
};

export default combineReducers({
    data: astroReducer,
});
