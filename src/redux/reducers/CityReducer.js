import { combineReducers } from 'redux';
import {
    FETCH_CITY_FAILED,
    FETCH_CITY_SUCCESS,
} from '../actions/CityActions';

const cityReducer = (state = {}, action) => {
    switch (action.type) {
        case FETCH_CITY_SUCCESS:
            return {
                ...state,
                ...action.response,
            };
        case FETCH_CITY_FAILED:
            return {
                ...state,
            };

        default:
            return state;
    }
};

export default combineReducers({
    data: cityReducer,
});
