import { combineReducers } from 'redux';
import appReducer from './appReducer';
import loginReducer from './LoginReducer';
import profileReducer from './ProfileReducer';
import astroReducer from './AstroReducer';
import friendReducer from './FriendReducer';
import cityReducer from './CityReducer';

export default combineReducers({
    app: appReducer,
    facebook: loginReducer,
    profile: profileReducer,
    astro: astroReducer,
    friend: friendReducer,
    city: cityReducer
});
