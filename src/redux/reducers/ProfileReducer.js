import {
    ACTION_FETCH_PROFILE_SUCCESS,
    ACTION_FETCH_PROFILE_FAILED,
    ACTION_FORM_DATA_CHANGE,
    ACTION_SAVE_SUCCESS,
    ACTION_SAVE_FAILED
} from '../actions/ProfileActions';
import { combineReducers } from 'redux';
import moment from 'moment';

const date = moment();

const formState = {
    data: {
        fullname: '',
        astroname: '',
        day: date.format('DD'),
        month: date.format('MM'),
        year: date.format('YYYY'),
        hour: date.format('hh'),
        minute: date.format('mm'),
        account: 0
    }
};

const profileReducer = (state = formState, action) => {
    switch (action.type) {
        case ACTION_FETCH_PROFILE_SUCCESS:
            return {
                ...state,
                ...action.response
            };

        case ACTION_FETCH_PROFILE_FAILED:
            return state;
        case ACTION_FORM_DATA_CHANGE:
            state.data[action.key] = action.value;
            return {
                ...state
            };
        case ACTION_SAVE_SUCCESS:
            return {
                ...state,
                ...action.profile.data,
                saved: true
            };
        case ACTION_SAVE_FAILED:
            return {
                ...state,
                saved: true
            };

        default:
            return state;
    }
};

const save = (state, profile) => {
    return {
        ...state,
        profile: profile
    };
};

export default combineReducers({
    data: profileReducer
});
