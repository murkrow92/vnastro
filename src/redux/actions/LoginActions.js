import { LoginManager, AccessToken } from 'react-native-fbsdk';
import { API } from '../../lib/API';
import AsyncStorageHelper from '../../helper/AsyncStorageHelper';

export const ACTION_ON_LOGIN_FACEBOOK_SUCCESS = 'action.facebook.login.success';
export const ACTION_ON_LOGIN_FACEBOOK_FAILED = 'action.facebook.login.failed';

export const ACTION_ON_LOGIN_VNASTRO_SUCCESS =
    'com.action.login.vnastro.success';
export const ACTION_ON_LOGIN_VNASTRO_FAILED = 'com.action.login.vnastro.failed';

export const loginFacebook = () => (dispatch, getState) => {
    LoginManager.logInWithReadPermissions([
        'public_profile',
        'email',
        'user_friends'
    ]).then(
        result => {
            console.log('result:', result);
            if (result.isCancelled) {
                dispatch(onLoginFacebookFailed());
            } else {
                AccessToken.getCurrentAccessToken().then(data => {
                    dispatch(
                        onLoginFacebookSuccess(data.accessToken.toString())
                    );
                });
            }
        },
        error => {
            console.log('Login failed with error: ' + error);
            dispatch(onLoginFacebookFailed());
        }
    );
};

export const fetchFacebookToken = () => (dispatch, getState) => {
    AccessToken.getCurrentAccessToken().then(data => {
        if (data != null) {
            const date = new Date();
            const time_stamp = date.getTime();
            if (data.expirationTime - time_stamp > 0) {
                dispatch(onLoginFacebookSuccess(data.accessToken));
            } else {
                AccessToken.refreshCurrentAccessTokenAsync().then(data1 => {
                    if (data1.accessToken) {
                        dispatch(onLoginFacebookSuccess(data1.accessToken));
                    } else {
                        dispatch(onLoginFacebookFailed());
                    }
                });
            }
        } else {
            dispatch(onLoginFacebookFailed());
        }
    });
};

const onLoginFacebookSuccess = accessToken => ({
    type: ACTION_ON_LOGIN_FACEBOOK_SUCCESS,
    accessToken
});

const onLoginFacebookFailed = () => ({
    type: ACTION_ON_LOGIN_FACEBOOK_FAILED
});

export const loginVnAstro = accessToken => (dispatch, getState) => {
    API.fetchFacebook(accessToken)
        .then(response => {
            API.login(response.email, response.id)
                .then(response1 => {
                    if (response1.done == 1) {
                        API.ACCESS_TOKEN = response1.access_token;
                        AsyncStorageHelper.saveVnastroToken(
                            response1.access_token
                        );
                        dispatch(
                            onLoginVnastroSuccess(
                                response,
                                response1.access_token
                            )
                        );
                    } else {
                        dispatch(onLoginVnastroFailed());
                    }
                })
                .catch(error => dispatch(onLoginVnastroFailed()));
        })
        .catch(error => {
            console.log('facebook profile: ', error, accessToken);
            dispatch(onLoginVnastroFailed());
        });
};

const onLoginVnastroSuccess = (facebook, vnastroToken) => ({
    type: ACTION_ON_LOGIN_VNASTRO_SUCCESS,
    facebook,
    vnastroToken
});

const onLoginVnastroFailed = () => ({
    type: ACTION_ON_LOGIN_VNASTRO_FAILED
});
