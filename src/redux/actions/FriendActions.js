import AsyncStorageHelper from '../../helper/AsyncStorageHelper';
import FriendHelper from '../../helper/FriendHelper';
import AlertHelper from '../../helper/AlertHelper';
import { API } from '../../lib/API';

export const FETCH_FRIEND_SUCCESS = 'com.action.fetch.friend.success';
export const FETCH_FRIEND_FAILED = 'com.action.fetch.friend.failed';
export const REFRESH_LIST_FRIEND = 'com.action.refresh.friend.list';
export const SYNC_FACEBOOK_SUCCESS = 'com.action.sync.facebook.success';
export const SYNC_FACEBOOK_FAILED = 'com.action.sync.facebook.failed';
export const SEARCH_FRIEND = 'com.action.search.friend';
export const ACTION_ADD_CONTACT_SUCCESS = 'com.action.add.contact.success';
export const ACTION_ADD_CONTACT_FAILED = 'com.action.add.contact.failed';

const onFetchFriendSuccess = friends => ({
    type: FETCH_FRIEND_SUCCESS,
    friends
});

const onFetchFriendFailed = error => ({
    type: FETCH_FRIEND_FAILED,
    error
});

export const fetchFriend = () => (dispatch, getState) =>
    API.fetchFriend().then(
        response => {
            if (response.done === 1) {
                dispatch(onFetchFriendSuccess(response));
            } else {
                dispatch(onFetchFriendFailed());
            }
        },
        error => {
            AsyncStorageHelper.getFriends().then(result => {
                if (result != null) {
                    dispatch(onFetchFriendSuccess(JSON.parse(result)));
                } else {
                    dispatch(onFetchFriendFailed(error));
                }
            });
            dispatch(onFetchFriendFailed(error));
        }
    );

export const refresh = () => ({
    type: REFRESH_LIST_FRIEND
});

export const syncWithFacebook = (vnastroFriends, facebookFriends) => (
    dispatch,
    getState
) => {
    const newFriends = FriendHelper.getNewFacebookFriends(
        vnastroFriends,
        facebookFriends
    );

    if (newFriends.length > 0) {
        API.addFacebookFriends(newFriends).then(result => {
            if (result.done === 1) {
                dispatch(onSyncFacebookSuccess());
            } else {
                dispatch(onSyncFacebookFailed());
            }
        });
    } else {
        dispatch(onSyncFacebookFailed());
    }
};

const onSyncFacebookSuccess = () => ({
    type: SYNC_FACEBOOK_SUCCESS
});

const onSyncFacebookFailed = () => ({
    type: SYNC_FACEBOOK_FAILED
});

export const search = text => (dispatch, getState) => ({
    type: SEARCH_FRIEND,
    text
});

export const addFriend = (friend, callback) => (dispatch, getState) => {
    API.addFriend(friend).then(result => {
        if (result.done === 1) {
            AlertHelper.alertUpdated(() => {
                callback();
                dispatch(onAddContactSuccess());
            });
        } else {
            AlertHelper.alertError(() => {
                dispatch(onAddContactFailed());
            });
        }
    });
};

const onAddContactSuccess = () => ({
    type: ACTION_ADD_CONTACT_SUCCESS
});

const onAddContactFailed = () => ({
    type: ACTION_ADD_CONTACT_FAILED
});
