import { API } from '../../lib/API';
import AsyncStorageHelper from '../../helper/AsyncStorageHelper';

export const ACTION_SAVE_SUCCESS = 'com.action.save.success';
export const ACTION_SAVE_FAILED = 'com.action.save.failed';
export const ACTION_FORM_DATA_CHANGE = 'com.action.form.change';
export const ACTION_FETCH_PROFILE_SUCCESS = 'com.action.fetch.profile.success';
export const ACTION_FETCH_PROFILE_FAILED = 'com.action.fetch.profile.failed';

export const fetchProfile = () => (dispatch, getState) => {
    API.fetchUserProfile().then(response => {
        if (response.done == 1) {
            AsyncStorageHelper.saveUserProfile(response);
            dispatch(onFetchProfileSuccess(response));
        } else {
            AsyncStorageHelper.getUserProfile().then(result => {
                if (result != null) {
                    dispatch(onFetchProfileSuccess(JSON.parse(result)));
                } else {
                    dispatch(onFetchProfileFailed());
                }
            });
        }
    });
};

const onFetchProfileSuccess = response => ({
    type: ACTION_FETCH_PROFILE_SUCCESS,
    response
});

const onFetchProfileFailed = () => ({
    type: ACTION_FETCH_PROFILE_FAILED
});

export const updateProfile = profile => (dispatch, getState) => {
    API.saveProfile(profile.data.data)
        .then(response => {
            if (response.done == 1) {
                AlertHelper.alertUpdated(() => dispatch(saveSuccess(profile)));
            } else {
                AlertHelper.alertError(() => dispatch(saveFailed()));
            }
        })
        .catch(error => {
            console.log(error);
            AlertHelper.alertError(() => dispatch(saveFailed()));
        });
};

const saveSuccess = profile => ({
    type: ACTION_SAVE_SUCCESS,
    profile
});

const saveFailed = () => ({
    type: ACTION_SAVE_FAILED
});

export const onFormChange = (key, value) => ({
    type: ACTION_FORM_DATA_CHANGE,
    key,
    value
});
