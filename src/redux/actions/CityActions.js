import AsyncStorageHelper from '../../helper/AsyncStorageHelper';
import { API } from '../../lib/API';

export const FETCH_CITY_SUCCESS = 'action.fetch.city.success';
export const FETCH_CITY_FAILED = 'action.fetch.city.failed';

export const fetchCity = () => (dispatch, getState) => {
    API.fetchCity()
        .then(
            response => {
                AsyncStorageHelper.saveCity(response);
                dispatch(onFetchCitySuccess(response));
            },
            error => {
                AsyncStorageHelper.getCity().then(result => {
                    if (result != null) {
                        dispatch(onFetchCitySuccess(JSON.parse(result)));
                    } else {
                        dispatch(onFetchCityFailed());
                    }
                });
                dispatch(onFetchCityFailed(error));
            }
        )
        .catch(error => dispatch(onFetchCityFailed(error)));
};

const onFetchCitySuccess = response => ({
    type: FETCH_CITY_SUCCESS,
    response
});

const onFetchCityFailed = error => ({
    type: FETCH_CITY_FAILED,
    error
});
