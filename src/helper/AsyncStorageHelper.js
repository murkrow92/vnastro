import { AsyncStorage } from 'react-native';

const KEY_FACEBOOK_TOKEN = 'facebook_token';
const KEY_FACEBOOK_PROFILE = 'facebook_profile';
const KEY_PROFILE = 'key.user.profile';
const KEY_ASTRO_CURRENT = 'key.astro.current';
const KEY_ASTRO = 'com.key.astro';
const KEY_FRIENDS = 'com.key.friends';
const KEY_CITY = 'com.key.city';
const KEY_BASE = 'com.key.base';
const KEY_TIMELINE = 'com.key.timeline';
const KEY_COMBO_MEAN = 'com.key.combo.mean';
const KEY_VNASTRO_TOKEN = 'com.key.vnastro.token';

export default class AsyncStorageHelper {
    static saveFacebookToken(response) {
        AsyncStorage.setItem(KEY_FACEBOOK_TOKEN, JSON.stringify(response));
    }

    static async getFacebookToken() {
        const token = await AsyncStorage.getItem(KEY_FACEBOOK_TOKEN);
        if (token != null) {
            return JSON.parse(token);
        }
    }

    static removeFacebookToken() {
        AsyncStorage.removeItem(KEY_FACEBOOK_TOKEN);
    }

    static saveVnastroToken(response) {
        AsyncStorage.setItem(KEY_VNASTRO_TOKEN, JSON.stringify(response));
    }

    static getVnastroToken() {
        return AsyncStorage.getItem(KEY_VNASTRO_TOKEN);
    }

    static saveFacebookProfile(response) {
        AsyncStorage.setItem(KEY_FACEBOOK_PROFILE, JSON.stringify(response));
    }

    static async getFacebookProfile() {
        return await AsyncStorage.getItem(KEY_FACEBOOK_PROFILE);
    }

    static saveAstro(response, key_time) {
        AsyncStorage.setItem(KEY_ASTRO + key_time, JSON.stringify(response));
    }

    static async getAstro(key_time) {
        return AsyncStorage.getItem(KEY_ASTRO + key_time);
    }

    static removeAstro(key_time) {
        AsyncStorage.removeItem(KEY_ASTRO + key_time);
    }

    static saveCurrent(response) {
        AsyncStorage.setItem(KEY_ASTRO_CURRENT, JSON.stringify(response));
    }

    static async getCurrent() {
        return await AsyncStorage.getItem(KEY_ASTRO_CURRENT);
    }

    static saveUserProfile(data) {
        AsyncStorage.setItem(KEY_PROFILE, JSON.stringify(data));
    }

    static async getUserProfile() {
        return await AsyncStorage.getItem(KEY_PROFILE);
    }

    static saveFriends(data) {
        AsyncStorage.setItem(KEY_FRIENDS, JSON.stringify(data));
    }

    static async getFriends() {
        return await AsyncStorage.getItem(KEY_FRIENDS);
    }

    static saveCity(data) {
        AsyncStorage.setItem(KEY_CITY, JSON.stringify(data));
    }

    static async getCity() {
        return await AsyncStorage.getItem(KEY_CITY);
    }

    static saveBase(data) {
        AsyncStorage.setItem(KEY_BASE, JSON.stringify(data));
    }

    static async getBase() {
        return await AsyncStorage.getItem(KEY_BASE);
    }

    static saveTimeline(data) {
        AsyncStorage.setItem(KEY_TIMELINE, JSON.stringify(data));
    }

    static async getTimeline() {
        return await AsyncStorage.getItem(KEY_TIMELINE);
    }

    static saveComboMean(comboString, data) {
        AsyncStorage.setItem(
            KEY_COMBO_MEAN + comboString,
            JSON.stringify(data)
        );
    }

    static async getComboMean(comboString) {
        return await AsyncStorage.getItem(KEY_COMBO_MEAN + comboString);
    }
}
