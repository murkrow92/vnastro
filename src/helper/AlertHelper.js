import { Alert } from 'react-native';

export default class AlertHelper {
    static alertUpdated(callback) {
        if (!callback) {
            callback = () => console.log('OK Pressed');
        }

        Alert.alert(
            'Thông báo',
            'Đã cập nhật',
            [{ text: 'OK', onPress: callback }],
            { cancelable: false },
        );
    }

    static alertError(callback) {
        if (!callback) {
            callback = () => console.log('OK Pressed');
        }
        Alert.alert(
            'Lỗi',
            'Đã có lỗi xảy ra',
            [{ text: 'OK', onPress: callback }],
            { cancelable: false },
        );
    }
}
