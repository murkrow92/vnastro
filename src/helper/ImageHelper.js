import BlankProfile from '../../assets/images/blank_profile.png';

export default class ImageHelper {
    static getPictureFromFacebookObject(facebookId, pictureObject = {}) {
        let picture = BlankProfile;
        if (typeof pictureObject === 'object' && facebookId !== '') {
            picture = {
                uri: `http://graph.facebook.com/${facebookId}/picture?type=large`
            };
        }
        return picture;
    }

    static getPictureFromPageUrl(url) {
        return url === '' ? BlankProfile : { uri: url };
    }
}
