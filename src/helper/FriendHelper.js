import lodash from 'lodash';
import StringHelper from './StringHelper';

const MAX_ITEM_PER_PAGE = 10;

export default class FriendHelper {
    static getContactFriend(friends, searchText) {
        const text = StringHelper.replaceVietnamese(searchText);
        const ls = [];
        lodash.forEach(friends, function(value, key) {
            value.key = key;
            const name = StringHelper.replaceVietnamese(value.name);
            if (value.fb_id === '' && (text === '' || name.includes(text))) {
                ls.push(value);
            }
        });
        return ls;
    }

    static getFacebookFriends(friends, searchText, page = 1) {
        const text = StringHelper.replaceVietnamese(searchText);
        const ls = [];
        lodash.forEach(friends, function(value, key) {
            const name = StringHelper.replaceVietnamese(value.name);
            if (text === '' || name.includes(text)) {
                ls.push(value);
            }
        });
        lodash.reverse(ls);
        const limit = MAX_ITEM_PER_PAGE * page;
        if (ls.length > limit) {
            return ls.slice(0, limit)
        }
        return ls;
    }

    static getNewFacebookFriends(vnastroFriends, facebookFriends) {
        let result = [];
        lodash.forEach(facebookFriends, function(value, key) {
            const facebookId = value.id;
            let existed = false;
            lodash.forEach(vnastroFriends, function(v, k) {
                if (v.fb_id == facebookId) {
                    existed = true;
                }
            });
            if (!existed) {
                value.fb_id = value.id;
                result.push(value);
            }
        });

        return result;
    }
}
