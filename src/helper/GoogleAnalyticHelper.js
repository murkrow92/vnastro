import {
    Analytics,
    Hits as GAHits,
    Experiment as GAExperiment
} from 'react-native-google-analytics';
import { GOOGLE_ANALYTIC_TRACKING_ID } from '../../config';
import DeviceInfo from 'react-native-device-info';

const clientId = DeviceInfo.getUniqueID();

const ga = new Analytics(
    GOOGLE_ANALYTIC_TRACKING_ID,
    clientId,
    1,
    DeviceInfo.getUserAgent()
);

export default class GoogleAnalyticHelper {
    static hitScreen(screenName) {
        const screenView = new GAHits.ScreenView(
            'VnAstro',
            screenName,
            DeviceInfo.getReadableVersion(),
            DeviceInfo.getBundleId()
        );
        ga.send(screenView);
    }
}
