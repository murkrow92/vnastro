import React from 'react';
import { StackNavigator, TabNavigator } from 'react-navigation';
import { View } from 'react-native';

import CustomStatusBar from '../components/CustomStatusBar';

import LoginScreen from '../screens/LoginScreen';
import FriendScreen from '../screens/FriendScreen';
import AstroScreen from '../screens/AstroScreen';
import HomeScreen from '../screens/HomeScreen';
import ExitScreen from '../screens/ExitScreen';

import { ICON_SIZE_20 } from '../../styles/dimens';
import {
    COLOR_GREY_E4,
    COLOR_LIGHT_BLUE,
    COLOR_TAB_BAR_BLUE,
    COLOR_WHITE
} from '../../styles/colors';

import FontelloIcon from '../components/FontelloIcon';
import AdBanner from "../components/AdBanner";

const Tab = TabNavigator(
    {
        Home: {
            screen: HomeScreen,
            navigationOptions: {
                tabBarIcon: ({ tintColor }) => (
                    <FontelloIcon
                        style={{ fontSize: 40 }}
                        name="home"
                        size={ICON_SIZE_20}
                        color={tintColor}
                    />
                )
            }
        },
        Astro: {
            screen: AstroScreen,
            navigationOptions: {
                tabBarIcon: ({ tintColor }) => (
                    <FontelloIcon
                        name="sun-inv"
                        size={ICON_SIZE_20}
                        color={tintColor}
                    />
                )
            }
        },
        Friend: {
            screen: FriendScreen,
            navigationOptions: {
                tabBarIcon: ({ tintColor }) => (
                    <FontelloIcon
                        name="group"
                        size={ICON_SIZE_20}
                        color={tintColor}
                    />
                )
            }
        },
        Exit: {
            screen: ExitScreen,
            navigationOptions: {
                tabBarIcon: ({ tintColor }) => (
                    <FontelloIcon
                        name="logout"
                        size={ICON_SIZE_20}
                        color={tintColor}
                    />
                )
            }
        }
    },
    {
        tabBarPosition: 'top',
        swipeEnabled: true,
        animationEnabled: true,
        lazy: true,
        tabBarOptions: {
            initialRouteName: 'Home',
            activeTintColor: COLOR_WHITE,
            inactiveTintColor: COLOR_WHITE,
            indicatorStyle: {
                backgroundColor: COLOR_LIGHT_BLUE
            },
            swipeEnabled: true,
            showLabel: false,
            showIcon: true,
            style: {
                borderTopColor: 'transparent',
                borderWidth: 1,
                backgroundColor: COLOR_TAB_BAR_BLUE,
                height: 50,
                borderColor: COLOR_GREY_E4
            }
        }
    }
);

const MainTabScreen = ({}) => (
    <View style={{ flex: 1 }}>
        <CustomStatusBar />
        <Tab />
        <AdBanner/>
    </View>
);

export default (AppStack = StackNavigator(
    {
        Login: {
            screen: LoginScreen
        },
        Astro: {
            screen: AstroScreen
        },

        Tab: {
            screen: MainTabScreen
        }
    },
    {
        headerMode: 'none'
    }
));
