import React from 'react';
import {
    FlatList,
    Image,
    ImageBackground,
    ScrollView,
    Text,
    TouchableOpacity,
    View,
    Linking
} from 'react-native';
import styles from './styles';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as profileActions from '../../redux/actions/ProfileActions';
import ImageHelper from '../../helper/ImageHelper';
import moment from 'moment';
import GoogleAnalyticHelper from '../../helper/GoogleAnalyticHelper';
import { wizyFacebookID } from '../../../config';
import FontelloIcon from '../../components/FontelloIcon';
import {
    COLOR_DARK_33,
    COLOR_GREY_99,
    COLOR_GREY_E4
} from '../../../styles/colors';
import {
    openSans13Dark33
} from '../../../styles/styles';

import i1 from '../../../assets/images/i1.png';
import i2 from '../../../assets/images/i2.png';
import i9 from '../../../assets/images/i9.png';
import i12 from '../../../assets/images/i12.png';
import i14 from '../../../assets/images/i14.png';
import i25 from '../../../assets/images/i25.png';
import i51 from '../../../assets/images/i51.png';
import Aries from '../../../assets/images/signs/1.png';
import Taurus from '../../../assets/images/signs/2.png';
import Gemini from '../../../assets/images/signs/3.png';
import Cancer from '../../../assets/images/signs/4.png';
import Leo from '../../../assets/images/signs/5.png';
import Virgo from '../../../assets/images/signs/6.png';
import Libra from '../../../assets/images/signs/7.png';
import Scorpio from '../../../assets/images/signs/8.png';
import Sagittarius from '../../../assets/images/signs/9.png';
import Capricorn from '../../../assets/images/signs/10.png';
import Aquarius from '../../../assets/images/signs/11.png';
import Pisces from '../../../assets/images/signs/12.png';
import TarotCourse from '../../../assets/images/tarot_course.jpg';
import AstroCourse from '../../../assets/images/astro_course.jpg';
import ForbiddenForest from '../../../assets/images/ForbiddenForest.jpg';

const wizyAvatar = ImageHelper.getPictureFromFacebookObject(wizyFacebookID, {});

class HomeScreen extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <ScrollView>
                <View style={styles.container}>
                    {this.renderProfile()}
                    <Text style={styles.header}>Dịch vụ</Text>
                    {this.renderServiceArea1()}
                    {this.renderServiceArea2()}
                    <Text style={styles.title}>Những người đã được xem</Text>
                    {this.renderCustomer()}
                    <Text style={styles.header}>Tìm hiểu huyền thuật</Text>
                    {this.renderClass()}
                    <Text style={styles.title}>Học viện</Text>
                    {this.renderSchoolArea()}
                    <Text style={styles.title}>Các nhà trong học viện</Text>
                    {this.renderHouse()}
                    <Text style={styles.title}>Sinh vật huyền bí</Text>
                    {this.renderMythicalBiology1()}
                    {this.renderMythicalBiology2()}
                    <Text style={styles.title}>Bạn bè</Text>
                    {this.renderFriend()}
                </View>
            </ScrollView>
        );
    }

    renderProfile = () => {
        const { facebook, profile } = this.props;
        const { id, picture } = facebook.data;
        const avatar = ImageHelper.getPictureFromFacebookObject(id, picture);
        const {
            fullname,
            astroname,
            day,
            month,
            year,
            account
        } = profile.data.data;
        const date = moment(`${day}/${month}/${year}`, 'DD-MM-YYYY').format(
            'DD/MM/YYYY'
        );

        return (
            <View style={styles.containerPadding10}>
                <Image style={styles.avatar} source={avatar} />
                <View style={styles.nameContainer}>
                    <Text style={styles.fullname}>{fullname}</Text>
                    <Text style={styles.birthdate}>Ngày sinh {date}</Text>
                </View>
                <View style={styles.accountContainer}>
                    <Text style={styles.accountLabel}>Tài khoản</Text>
                    <Text style={styles.accountContent}>
                        {account} 30.000 AC
                    </Text>
                </View>
            </View>
        );
    };

    renderSchoolArea = () => (
        <View style={styles.schoolContainer}>
            <TouchableOpacity style={styles.area}>
                <Image style={styles.schoolAreaIcon} source={i25} />
                <Text style={styles.schoolAreaTitle}>Sảnh đường</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.area}>
                <Image source={i2} style={styles.schoolAreaIcon} />
                <Text style={styles.schoolAreaTitle}>
                    Hẻm không mua thì Xéo
                </Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.area}>
                <Image source={i14} style={styles.schoolAreaIcon} />
                <Text style={styles.schoolAreaTitle}>Rừng cấm</Text>
            </TouchableOpacity>
            <TouchableOpacity
                onPress={() =>
                    Linking.openURL('https://www.facebook.com/wizy.sanjay')}
                style={styles.area}
            >
                <Image
                    source={wizyAvatar}
                    style={[
                        styles.schoolAreaIcon,
                        {
                            borderRadius: 20
                        }
                    ]}
                />
                <Text style={styles.schoolAreaTitle}>Hiệu trưởng Sắn Dây</Text>
            </TouchableOpacity>
        </View>
    );

    renderClass = () => (
        <FlatList
            keyExtractor={(item, index) => item.name}
            horizontal={true}
            style={{ flex: 1 }}
            data={courses}
            renderItem={({ item }) => <ClassItem item={item} />}
        />
    );

    renderHouse = () => (
        <View style={styles.schoolContainer}>
            <TouchableOpacity style={styles.area}>
                <Image style={styles.houseIcon} source={i12} />
                <Text style={styles.schoolAreaTitle}>Nhà lửa</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.area}>
                <Image source={i9} style={styles.houseIcon} />
                <Text style={styles.schoolAreaTitle}>Nhà đất</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.area}>
                <Image source={i1} style={styles.houseIcon} />
                <Text style={styles.schoolAreaTitle}>Nhà khí</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.area}>
                <Image source={i51} style={styles.houseIcon} />
                <Text style={styles.schoolAreaTitle}>Nhà nước</Text>
            </TouchableOpacity>
        </View>
    );

    renderMythicalBiology1 = () => (
        <View style={styles.schoolContainer}>
            <TouchableOpacity style={styles.area}>
                <Image style={styles.schoolAreaIcon} source={Aries} />
            </TouchableOpacity>
            <TouchableOpacity style={styles.area}>
                <Image style={styles.schoolAreaIcon} source={Taurus} />
            </TouchableOpacity>
            <TouchableOpacity style={styles.area}>
                <Image style={styles.schoolAreaIcon} source={Gemini} />
            </TouchableOpacity>
            <TouchableOpacity style={styles.area}>
                <Image style={styles.schoolAreaIcon} source={Cancer} />
            </TouchableOpacity>
            <TouchableOpacity style={styles.area}>
                <Image style={styles.schoolAreaIcon} source={Leo} />
            </TouchableOpacity>
            <TouchableOpacity style={styles.area}>
                <Image style={styles.schoolAreaIcon} source={Virgo} />
            </TouchableOpacity>
        </View>
    );

    renderMythicalBiology2 = () => (
        <View style={styles.schoolContainer}>
            <TouchableOpacity style={styles.area}>
                <Image style={styles.schoolAreaIcon} source={Libra} />
            </TouchableOpacity>
            <TouchableOpacity style={styles.area}>
                <Image style={styles.schoolAreaIcon} source={Scorpio} />
            </TouchableOpacity>
            <TouchableOpacity style={styles.area}>
                <Image style={styles.schoolAreaIcon} source={Sagittarius} />
            </TouchableOpacity>
            <TouchableOpacity style={styles.area}>
                <Image style={styles.schoolAreaIcon} source={Capricorn} />
            </TouchableOpacity>
            <TouchableOpacity style={styles.area}>
                <Image style={styles.schoolAreaIcon} source={Aquarius} />
            </TouchableOpacity>
            <TouchableOpacity style={styles.area}>
                <Image style={styles.schoolAreaIcon} source={Pisces} />
            </TouchableOpacity>
        </View>
    );

    renderServiceArea1 = () => (
        <View style={styles.schoolContainer}>
            <TouchableOpacity style={styles.area}>
                <FontelloIcon
                    name="star-empty"
                    size={40}
                    color={COLOR_GREY_99}
                />
                <Text style={styles.serviceTitle}>Tính cách cá nhân</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.area}>
                <FontelloIcon
                    name="chart-bar"
                    size={40}
                    color={COLOR_GREY_99}
                />
                <Text style={styles.serviceTitle}>Định hướng nghề nghiệp</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.area}>
                <FontelloIcon name="heart" size={40} color={COLOR_GREY_99} />
                <Text style={styles.serviceTitle}>Tình yêu</Text>
            </TouchableOpacity>
        </View>
    );

    renderServiceArea2 = () => (
        <View style={styles.schoolContainer}>
            <TouchableOpacity style={styles.area}>
                <FontelloIcon name="puzzle" size={40} color={COLOR_GREY_99} />
                <Text style={styles.serviceTitle}>Giáo dục con cái</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.area}>
                <FontelloIcon name="group" size={40} color={COLOR_GREY_99} />
                <Text style={styles.serviceTitle}>Các mối quan hệ</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.area}>
                <FontelloIcon
                    name="cavoi_svg"
                    size={40}
                    color={COLOR_GREY_99}
                />
                <Text style={styles.serviceTitle}>
                    Xem dịch vụ theo yêu cầu
                </Text>
            </TouchableOpacity>
        </View>
    );

    renderCustomer = () => {
        return (
            <FlatList
                keyExtractor={(item, index) => item.name}
                horizontal={true}
                style={{ flex: 1 }}
                data={[
                    sampleUser,
                    sampleUser2,
                    sampleUser3,
                    sampleUser4,
                    sampleUser5
                ]}
                renderItem={({ item }) => {
                    return <UserItem item={item} />;
                }}
            />
        );
    };

    renderFriend = () => {
        return (
            <FlatList
                keyExtractor={(item, index) => item.name}
                horizontal={true}
                style={{ flex: 1 }}
                data={[
                    addUser,
                    sampleUser,
                    sampleUser2,
                    sampleUser3,
                    sampleUser4,
                    sampleUser5
                ]}
                renderItem={({ item }) => {
                    return item.name === 'add' ? (
                        <AddItem />
                    ) : (
                        <UserItem item={item} />
                    );
                }}
            />
        );
    };

    componentDidMount() {
        const { profileActions } = this.props;
        GoogleAnalyticHelper.hitScreen('Home Screen');
        profileActions.fetchProfile();
    }

    componentDidUpdate() {}
}

const courses = [
    { bg: TarotCourse, name: 'Chiết tâm bài thuật' },
    { bg: AstroCourse, name: 'Chiêm tinh thuật' },
    { bg: ForbiddenForest, name: 'Sinh vật huyền bí' }
];

const ClassItem = ({ item }) => (
    <TouchableOpacity style={{ padding: 10 }}>
        <ImageBackground source={item.bg} style={styles.courseIcon}>
            <Text style={styles.courseTitle}>{item.name}</Text>
        </ImageBackground>
    </TouchableOpacity>
);

const addUser = {
    name: 'add'
};

const sampleUser = {
    name: 'Đoàn Phúc Bảo',
    facebookID: wizyFacebookID
};

const sampleUser2 = {
    name: 'Đoàn Phúc Bảo 2',
    facebookID: wizyFacebookID
};

const sampleUser3 = {
    name: 'Đoàn Phúc Bảo 3',
    facebookID: wizyFacebookID
};

const sampleUser4 = {
    name: 'Đoàn Phúc Bảo 4',
    facebookID: wizyFacebookID
};

const sampleUser5 = {
    name: 'Đoàn Phúc Bảo 5',
    facebookID: wizyFacebookID
};

const AddItem = ({}) => (
    <TouchableOpacity
        style={{
            paddingVertical: 10,
            alignItems: 'center'
        }}
    >
        <View
            style={{
                alignItems: 'center',
                justifyContent: 'center',
                width: 60,
                height: 60
            }}
        >
            <FontelloIcon color={COLOR_DARK_33} name="plus-circle" size={36} />
        </View>

        <Text
            style={[
                openSans13Dark33,
                {
                    marginTop: 5,
                    width: '75%',
                    textAlign: 'center'
                }
            ]}
        >
            Thêm bạn
        </Text>
    </TouchableOpacity>
);

const UserItem = ({ item }) => (
    <TouchableOpacity
        style={{
            paddingVertical: 10,
            alignItems: 'center'
        }}
    >
        <Image
            style={{
                width: 60,
                height: 60,
                borderRadius: 30,
                borderWidth: 1,
                borderColor: COLOR_GREY_E4
            }}
            source={ImageHelper.getPictureFromFacebookObject(item.facebookID)}
        />
        <Text
            style={[
                openSans13Dark33,
                {
                    marginTop: 5,
                    width: '75%',
                    textAlign: 'center'
                }
            ]}
        >
            {item.name}
        </Text>
    </TouchableOpacity>
);

const mapStateToProps = state => ({
    facebook: state.facebook,
    profile: state.profile
});

const mapDispatchToProps = dispatch => ({
    profileActions: bindActionCreators(profileActions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
