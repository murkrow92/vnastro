import { StyleSheet } from 'react-native';
import {
    area,
    arialWhite15,
    container,
    containerPadding10,
    openSans12Grey,
    openSans13Dark33,
    openSans14Dark,
    openSans18Dark33
} from '../../../styles/styles';
import {
    COLOR_DARK_33,
    COLOR_GREY_E4,
    COLOR_GREY_F2
} from '../../../styles/colors';

export default (styles = StyleSheet.create({
    container: container,
    containerPadding10: containerPadding10,
    avatar: {
        borderWidth: 1,
        borderColor: COLOR_GREY_E4,
        marginLeft: 10,
        width: 50,
        height: 50,
        borderRadius: 25
    },
    nameContainer: {
        marginLeft: 10,
        flexDirection: 'column'
    },
    fullname: openSans14Dark,
    birthdate: openSans12Grey,
    accountContainer: {
        marginLeft: 10,
        marginRight: 10,
        flex: 1,
        marginLeft: 10,
        flexDirection: 'column',
        alignItems: 'flex-end'
    },
    accountLabel: openSans12Grey,
    accountContent: openSans18Dark33,
    schoolContainer: containerPadding10,
    area: area,
    schoolAreaIcon: {
        width: 40,
        height: 40
    },
    schoolAreaTitle: {
        marginTop: 5,
        paddingLeft: 10,
        paddingRight: 10,
        fontFamily: 'Helvetica',
        color: COLOR_DARK_33,
        fontSize: 12,
        textAlign: 'center'
    },
    header:{
        ...containerPadding10,
        ...openSans13Dark33,
        backgroundColor: COLOR_GREY_F2,
        paddingLeft: 10,
        fontWeight:'bold'
    },
    title: {
        ...containerPadding10,
        ...openSans13Dark33,
        backgroundColor: COLOR_GREY_F2,
        paddingLeft: 10
    },
    courseIcon: {
        padding: 10,
        width: 114,
        height: 70,
        alignItems: 'center',
        justifyContent: 'center'
    },
    courseTitle: {
        ...arialWhite15,
        fontWeight: 'bold',
        textAlign: 'center',
        paddingHorizontal: 10
    },
    houseIcon: {
        width: 32,
        height: 32,
        borderRadius: 16,
        borderWidth: 1,
        borderColor: COLOR_GREY_E4
    },
    serviceTitle: {
        marginTop: 5,
        paddingLeft: 25,
        paddingRight: 25,
        fontFamily: 'Helvetica',
        color: COLOR_DARK_33,
        fontSize: 13,
        textAlign: 'center'
    }
}));
