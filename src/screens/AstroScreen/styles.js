import { StyleSheet } from 'react-native';
import { container } from '../../../styles/styles';

export default (styles = StyleSheet.create({
    container: {
        ...container,
        justifyContent: 'flex-end'
    }
}));
