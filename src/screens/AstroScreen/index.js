import React from 'react';
import { View, WebView } from 'react-native';
import styles from './styles';
import GoogleAnalyticHelper from '../../helper/GoogleAnalyticHelper';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as profileActions from '../../redux/actions/ProfileActions';
import * as astroActions from '../../redux/actions/AstroActions';
import chart from '../../lib/chart/index.html';

class AstroScreen extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={styles.container}>
                <WebView
                    ref="webView"
                    javaScriptEnabledAndroid={true}
                    scrollEnabled={false}
                    scalesPageToFit={true}
                    source={chart}
                    renderLoading={error => {
                        console.log('loading error:', error);
                    }}
                    onLoad={() => {
                        console.log('on load: ');
                        this.refs.webView.postMessage('Hello');
                    }}
                />
            </View>
        );
    }

    componentDidMount() {
        GoogleAnalyticHelper.hitScreen('Astro Screen');
        const { astroActions, profile } = this.props;
        const { city_id } = profile.data.data;
        astroActions.fetchAstro(new Date(), city_id);
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.astro.data !== this.props.astro) {
            if (this.props.astro.data.data) {
                console.log('drawing', this.props.astro.data.data);
                this.refs.webView.postMessage(this.props.astro.data.data);
            }
        }
    }
}

const mapStateToProps = state => ({
    facebook: state.facebook,
    profile: state.profile,
    astro: state.astro
});

const mapDispatchToProps = dispatch => ({
    profileActions: bindActionCreators(profileActions, dispatch),
    astroActions: bindActionCreators(astroActions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(AstroScreen);
