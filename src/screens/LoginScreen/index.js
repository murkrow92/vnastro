import React from 'react';
import {
    Image,
    Text,
    TouchableOpacity,
    View,
    ActivityIndicator
} from 'react-native';
import Logo from '../../../assets/images/vnastro_large.png';
import FBLogo from '../../../assets/images/fb_logo.png';
import styles from './styles';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as loginActions from '../../redux/actions/LoginActions';
import * as cityActions from '../../redux/actions/CityActions';

import {
    COLOR_DARK_33,
    COLOR_GREY_AE,
    COLOR_GREY_E4
} from '../../../styles/colors';
import Icon from '../../../assets/images/vnastro_large.png';

class LoginScreen extends React.Component {
    render() {
        const { loggedInFacebook } = this.props.facebook.data;
        return (
            <View style={styles.container}>
                <Image resizeMode="contain" style={styles.logo} source={Logo} />
                {loggedInFacebook
                    ? this.renderVnAstroButton()
                    : this.renderLoginFacebookButton()}
            </View>
        );
    }

    renderVnAstroButton() {
        const { loggingInVnAstro } = this.props.facebook.data;

        return loggingInVnAstro
            ? this.renderVnAstroButtonByText('Đang vào ứng dụng', () => {})
            : this.renderVnAstroButtonByText('Tiếp tục vào ứng dụng', () => {
                  this.doLoginVnAstro();
              });
    }

    renderVnAstroButtonByText(text, onPress) {
        const { loggingInVnAstro } = this.props.facebook.data;

        return (
            <TouchableOpacity
                style={[
                    styles.loginButton,
                    {
                        backgroundColor: COLOR_GREY_E4
                    }
                ]}
                onPress={onPress}
            >
                {loggingInVnAstro ? (
                    <ActivityIndicator
                        style={styles.fbLogo}
                        size="small"
                        color={COLOR_GREY_AE}
                    />
                ) : (
                    <Image source={Icon} style={styles.fbLogo} />
                )}
                <Text
                    style={[
                        styles.text,
                        {
                            color: COLOR_DARK_33
                        }
                    ]}
                >
                    {text}
                </Text>
            </TouchableOpacity>
        );
    }

    renderLoginFacebookButton() {
        return (
            <TouchableOpacity
                style={styles.loginButton}
                onPress={() => this.doLoginFacebook()}
            >
                <Image source={FBLogo} style={styles.fbLogo} />
                <Text style={styles.text}>Đăng nhập bằng Facebook</Text>
            </TouchableOpacity>
        );
    }

    componentDidMount() {
        const { loginActions, cityActions } = this.props;
        loginActions.fetchFacebookToken();
        cityActions.fetchCity();
    }

    componentDidUpdate(prevProps, prevState) {
        const {
            loggedInFacebook,
            loggingInVnAstro,
            loggedInVnAstro
        } = this.props.facebook.data;
        const { navigate } = this.props.navigation;

        if (loggedInVnAstro) {
            navigate('Tab');
        } else if (loggedInFacebook && loggingInVnAstro) {
            this.doLoginVnAstro();
        }
    }

    doLoginVnAstro() {
        const { loginActions } = this.props;
        const { accessToken } = this.props.facebook.data;
        loginActions.loginVnAstro(accessToken);
    }

    doLoginFacebook() {
        const { loginActions } = this.props;
        loginActions.loginFacebook();
    }
}

const mapStateToProps = state => ({
    facebook: state.facebook,
    city: state.city
});

const mapDispatchToProps = dispatch => ({
    loginActions: bindActionCreators(loginActions, dispatch),
    cityActions: bindActionCreators(cityActions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
