import { StyleSheet } from 'react-native';
import {
    COLOR_FACEBOOK_BOLD_BLUE,
    COLOR_GREY_BACKGROUND,
    COLOR_WHITE
} from '../../../styles/colors';
import { HelveticaNeueBold } from '../../../styles/fonts';

export default (styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLOR_GREY_BACKGROUND,
        alignItems: 'center'
    },
    logo: {
        width: '50%',
        height: '50%'
    },
    loginButton: {
        minWidth: 242,
        paddingRight: 20,
        flexDirection: 'row',
        borderRadius: 5,
        backgroundColor: COLOR_FACEBOOK_BOLD_BLUE,
        paddingTop: 10,
        paddingBottom: 10,
        alignItems: 'center'
    },
    fbLogo: {
        marginLeft: 10,
        width: 20,
        height: 20,
        marginRight: 10
    },
    text: {
        fontFamily: HelveticaNeueBold,
        fontSize: 13,
        color: COLOR_WHITE
    }
}));
