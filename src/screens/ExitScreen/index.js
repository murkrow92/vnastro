import React from 'react';
import { View } from 'react-native';
import styles from './styles';
import GoogleAnalyticHelper from '../../helper/GoogleAnalyticHelper';

export default class ExitScreen extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={styles.container}>
            </View>
        );
    }

    componentDidMount() {
        GoogleAnalyticHelper.hitScreen('Exit Screen');
    }
}
