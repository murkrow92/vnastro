import React from 'react';
import { View, StyleSheet, WebView } from 'react-native';
import chart from '../../lib/chart/index.html';
import GoogleAnalyticHelper from "../../helper/GoogleAnalyticHelper";

export default class ChartScreen extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <WebView
                    ref="webView"
                    scrollEnabled={false}
                    scalesPageToFit={true}
                    source={chart}
                    renderLoading={error => {
                        console.log('loading error:', error);
                    }}
                    onLoad={() => {
                        this.refs.webView.postMessage('Hello');
                    }}
                />
            </View>
        );
    }

    componentDidMount(){
        GoogleAnalyticHelper.hitScreen('Chart Screen');
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        justifyContent: 'center'
    }
});
