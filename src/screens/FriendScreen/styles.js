import { StyleSheet } from 'react-native';
import {
    COLOR_DARK_33,
    COLOR_GREY_E4,
    COLOR_WHITE,
} from '../../../styles/colors';
import { STATUS_BAR_HEIGHT } from '../../../styles/dimens';

export default (styles = StyleSheet.create({
    container: {
        marginTop: STATUS_BAR_HEIGHT,
        flex: 1,
        flexDirection: 'column',
        backgroundColor: COLOR_WHITE,
    },
    button: {
        flexDirection:'row',
        marginHorizontal: 15,
        borderRadius: 5,
        backgroundColor: COLOR_GREY_E4,
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 10,
        paddingBottom: 10,
        marginVertical: 10,
    },
    text: {
        marginLeft:10,
        fontFamily: 'Arial',
        fontWeight: 'bold',
        fontSize: 13,
        color: COLOR_DARK_33,
    },
}));
