import React from 'react';
import { View, FlatList, AppState } from 'react-native';
import styles from './styles';
import { bindActionCreators } from 'redux';
import * as actions from '../../redux/actions/FriendActions';
import { connect } from 'react-redux';
import FriendItem from './FriendItem';
import lodash from 'lodash';
import GoogleAnalyticHelper from '../../helper/GoogleAnalyticHelper';
import Divider from '../../components/Divider';
import FriendHelper from '../../helper/FriendHelper';
import { Entypo } from 'react-native-vector-icons';
import Searchbox from '../../components/Searchbox';

class FriendPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            searchText: '',
            page: 1
        };
    }
    render() {
        const { navigate } = this.props.navigation;
        const { friend } = this.props;
        let length = 0;
        if (friend.data.data) {
            length = lodash.size(friend.data.data);
        }
        return (
            <View style={styles.container}>
                <View>
                    <Searchbox
                        text={this.state.searchText}
                        search={text => this.search(text)}
                    />
                    <Divider />
                </View>
                {this.renderFlatList()}
            </View>
        );
    }

    renderFlatList() {
        const { friend, city } = this.props;
        const { navigate } = this.props.navigation;
        const { searchText, page } = this.state;
        const data = friend.data.data
            ? FriendHelper.getFacebookFriends(
                  friend.data.data,
                  searchText,
                  page
              )
            : [];

        return (
            <FlatList
                initialNumToRender={10}
                style={{
                    flex: 1
                }}
                keyExtractor={(item, index) => item.id}
                onRefresh={() => this.refresh()}
                refreshing={friend.data.refreshing || false}
                data={data}
                renderItem={({ item }) => (
                    <FriendItem item={item} navigate={navigate} city={city} />
                )}
                onEndReached={() => this.handleLoadMore()}
                onEndReachedThreshold={0.5}
            />
        );
    }

    handleLoadMore() {
        const { page } = this.state;
        this.setState({
            page: page + 1
        });
        this.fetchMoreFacebookFriends();
    }

    componentDidUpdate() {
        const { friend } = this.props;
        const { refreshing, facebookSynchronized } = friend.data;

        if (refreshing) {
            this.fetchFriend();
        } else if (!facebookSynchronized) {
            this.syncWithFacebook();
        }
    }

    syncWithFacebook() {
        const { actions, friend, facebook } = this.props;
        actions.syncWithFacebook(friend.data.data, facebook.data.friends.data);
    }

    fetchMoreFacebookFriends() {}

    search(text) {
        this.setState({
            searchText: text
        });
    }

    componentWillMount() {
        GoogleAnalyticHelper.hitScreen('Friend Screen');
    }

    componentDidMount() {
        this.fetchFriend();
        AppState.addEventListener(
            'change',
            this.handleAppStateChange.bind(this)
        );
    }

    fetchFriend() {
        const { actions } = this.props;
        actions.fetchFriend();
    }

    componentWillUnmount() {
        AppState.removeEventListener(
            'change',
            this.handleAppStateChange.bind(this)
        );
    }

    handleAppStateChange(nextAppState) {
        if (nextAppState === 'active') {
            this.refresh();
        }
    }

    refresh() {
        const { actions } = this.props;
        actions.refresh();
    }
}

const mapStateToProps = state => ({
    friend: state.friend,
    facebook: state.facebook,
    city: state.city
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(FriendPage);
