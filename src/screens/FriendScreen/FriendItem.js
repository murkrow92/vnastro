import React from 'react';
import { Image, Text, TouchableOpacity, View, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import {
    COLOR_DARK_33,
    COLOR_GREY_99,
    COLOR_GREY_E4,
    COLOR_PURPLE
} from '../../../styles/colors';
import ImageHelper from '../../helper/ImageHelper';
import CityHelper from '../../helper/CityHelper';
import { ICON_SIZE_14 } from '../../../styles/dimens';
import moment from 'moment';
import Divider from '../../components/Divider';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

export default class FriendItem extends React.Component {
    render() {
        const { item, city } = this.props;
        const {
            bday,
            bhour,
            bmonth,
            bminute,
            byear,
            fb_id,
            name,
            city_id
        } = item;

        const isUpdated = this.isUpdated();
        const text = isUpdated
            ? moment(
                  `${bday}-${bmonth}-${byear} ${bhour}:${bminute}`,
                  'DD-MM-YYYY HH:mm'
              ).format('DD/MM/YYYY HH:mm') +
              ' tại ' +
              CityHelper.getCityById(city, city_id).title
            : 'Chờ cập nhật ngày giờ sinh';

        return (
            <TouchableOpacity
                onPress={() => {
                    if (isUpdated) {
                        this.gotoAstro();
                    } else {
                        this.gotoFriendProfile();
                    }
                }}
            >
                <View style={styles.wrapper}>
                    <View style={styles.container}>
                        <Image
                            source={ImageHelper.getPictureFromFacebookObject(
                                fb_id,
                                {}
                            )}
                            style={styles.icon}
                        />
                        <View style={styles.contentContainer}>
                            <Text style={styles.title}>{name}</Text>
                            <Text style={styles.content}>{text}</Text>
                            {this.renderButton()}
                        </View>
                        {isUpdated ? (
                            <View />
                        ) : (
                            <FontAwesome
                                style={{
                                    alignSelf: 'center'
                                }}
                                size={ICON_SIZE_14}
                                color={COLOR_PURPLE}
                                name="chevron-right"
                            />
                        )}
                    </View>

                    <Divider color={COLOR_GREY_E4} />
                </View>
            </TouchableOpacity>
        );
    }

    renderButton() {
        if (this.isUpdated()) {
            return (
                <View style={styles.buttonContainer}>
                    <TouchableOpacity
                        style={styles.button}
                        onPress={() => this.gotoFriendProfile()}
                    >
                        <Text style={styles.buttonLabel}>Xem thông tin</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={[
                            styles.button,
                            {
                                marginLeft: 10
                            }
                        ]}
                        onPress={() => this.gotoAstro()}
                    >
                        <Text style={styles.buttonLabel}>Xem bản đồ sao</Text>
                    </TouchableOpacity>
                </View>
            );
        }
    }

    gotoAstro() {
        const { item, navigate } = this.props;
        const { bday, bhour, bmonth, bminute, byear, name, id, city_id } = item;
        if (this.isUpdated()) {
            navigate('Astro', {
                contact_id: id,
                city_id: city_id,
                day: bday,
                month: bmonth,
                year: byear,
                hour: bhour,
                minute: bminute,
                name: name
            });
        }
    }

    isUpdated() {
        const { item } = this.props;
        const { bday, bmonth, byear } = item;
        return bday != 0 && bmonth != 0 && byear != 0 && this.isPermitted();
    }

    isPermitted() {
        const { item } = this.props;
        const { permit, own } = item;
        return own == 2 || permit == 2;
    }

    gotoFriendProfile() {
        const { item, navigate } = this.props;
        let {
            id,
            bday,
            bhour,
            bmonth,
            bminute,
            byear,
            name,
            city_id,
            fb_id
        } = item;

        if (!this.isPermitted()) {
            bday = 0;
            bhour = 0;
            bmonth = 0;
            byear = 0;
            bminute = 0;
        }

        navigate('FriendProfile', {
            id: id,
            fb_id: fb_id,
            contact_id: id,
            city_id: city_id,
            day: bday,
            month: bmonth,
            year: byear,
            hour: bhour,
            minute: bminute,
            name: name
        });
    }
}

FriendItem.propTypes = {
    item: PropTypes.object.isRequired,
    navigate: PropTypes.func.isRequired
};

const styles = StyleSheet.create({
    wrapper: {
        flexDirection: 'column'
    },
    container: {
        marginTop: 10,
        marginBottom: 10,
        marginLeft: 15,
        marginRight: 15,
        flex: 1,
        flexDirection: 'row'
    },
    icon: {
        borderRadius: 5,
        marginRight: 10,
        width: 40,
        height: 40
    },
    contentContainer: {
        marginLeft: 5,
        flex: 1
    },
    title: {
        fontFamily: 'OpenSans',
        fontSize: 13,
        color: COLOR_DARK_33
    },
    content: {
        fontFamily: 'OpenSans',
        fontSize: 13,
        marginTop: 5,
        color: COLOR_GREY_99
    },
    buttonContainer: {
        paddingTop: 5,
        paddingBottom: 5,
        flex: 1,
        flexDirection: 'row'
    },
    button: {
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 5,
        paddingBottom: 5,
        borderRadius: 5,
        backgroundColor: COLOR_GREY_E4,
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonLabel: {
        fontFamily: 'Arial',
        color: COLOR_DARK_33,
        fontSize: 13,
        fontWeight: 'bold'
    }
});
