import { API_ENDPOINT } from '../../config';
import lodash from 'lodash';
import queryString from 'query-string';

const RESPONSE_CODE_SUCCESS = 200;

const checkStatus = response => {
    let json = response.json();
    if (response.status === RESPONSE_CODE_SUCCESS) {
        return json;
    } else {
        console.log(response);
        return json.then(Promise.reject.bind(Promise));
    }
};

const postRequest = (url, body, headers) =>
    fetch(url, { method: 'POST', body, headers })
        .then(checkStatus)
        .catch(error => console.log(error));

const getRequest = (url, headers) => fetch(url, { headers }).then(checkStatus);

const putRequest = (url, body, headers) =>
    fetch(url, { method: 'PUT', body, headers }).then(checkStatus);

export class API {
    static ACCESS_TOKEN = '';
    static API_ENDPOINT = API_ENDPOINT;

    static header() {
        return new Headers({
            Authorization: API.ACCESS_TOKEN
        });
    }

    static fetchFacebook(facebookToken) {
        const url = `https://graph.facebook.com/me?access_token=${facebookToken}&fields=id,name,picture,friends,email`;
        return getRequest(url);
    }

    static login(email, facebookId) {
        const body = new FormData();
        body.append('email', email);
        body.append('fb_id', facebookId);
        let url = `${this.API_ENDPOINT}/user/fbconnect`;
        return postRequest(url, body);
    }

    static fetchAstro(datetime, city_id) {
        let c = city_id == 0 ? 22 : city_id;
        let query = queryString.stringify({
            year: datetime.getFullYear(),
            month: datetime.getMonth() + 1,
            day: datetime.getDate(),
            hour: datetime.getHours(),
            minute: datetime.getMinutes(),
            city_id: c
        });

        return getRequest(`${this.API_ENDPOINT}/astro?${query}`, API.header());
    }

    static fetchNotifications() {
        return getRequest(`${this.API_ENDPOINT}/notify`, API.header());
    }

    static fetchConversations() {
        let url = `${this.API_ENDPOINT}/conversation`;
        return getRequest(url, API.header());
    }

    static fetchConversation(conversationId) {
        if (conversationId === '0') {
            conversationId = 1;
        }
        let query = queryString.stringify({
            id: conversationId
        });
        let url = `${this.API_ENDPOINT}/conversation?${query}`;
        return getRequest(url, API.header());
    }

    static fetchDetailCombo(combo) {
        const body = new FormData();
        lodash.forEach(combo, function(value, key) {
            body.append(key, value);
        });

        let url = `${this.API_ENDPOINT}/astro/mean`;
        return postRequest(url, body, API.header());
    }

    static fetchFriend() {
        const url = `${this.API_ENDPOINT}/contact`;
        return getRequest(url, API.header());
    }

    static addFriend(friend) {
        const body = new FormData();
        lodash.forEach(friend, function(value, key) {
            body.append(key, value);
        });

        const url = `${this.API_ENDPOINT}/contact/add`;
        return postRequest(url, body, API.header());
    }

    static fetchTransaction(userId) {
        userId = 2;
        let query = queryString.stringify({
            user_id: userId,
            status: 1
        });
        let url = `${this.API_ENDPOINT}/transaction?${query}`;
        return getRequest(url, API.header());
    }

    static saveProfile(profile) {
        const body = new FormData();
        const keys = [
            'id',
            'email',
            'day',
            'month',
            'hour',
            'minute',
            'mobile_phone',
            'year',
            'city_id'
        ];
        lodash.forEach(keys, function(value, key) {
            let field = value;

            if (value === 'id') {
                field = 'user_id';
            }

            if (value == 'mobile_phone') {
                field = 'phone';
            }

            if (profile.hasOwnProperty(value)) {
                body.append(field, profile[value]);
            }
        });

        const url = `${this.API_ENDPOINT}/user/update`;
        return postRequest(url, body, API.header());
    }

    static fetchUserProfile() {
        const url = `${this.API_ENDPOINT}/user`;
        return getRequest(url, API.header());
    }

    static addConversation(message) {
        const url = `${this.API_ENDPOINT}/conversation/add`;
        const body = new FormData();
        body.append('message', message);
        return postRequest(url, body, API.header());
    }

    static addMessage(conversationId, message) {
        const url = `${this.API_ENDPOINT}/conversation/addMessage`;
        const body = new FormData();
        body.append('conversation_id', conversationId);
        body.append('message', message);
        return postRequest(url, body, API.header());
    }

    static fetchCity() {
        const url = `${this.API_ENDPOINT}/astro/city`;
        const body = new FormData();
        body.append('test', '');
        return postRequest(url, body, API.header());
    }

    static addFacebookFriends(newFriends) {
        const url = `${this.API_ENDPOINT}/contact/addMulti`;
        const body = new FormData();
        body.append('contacts', JSON.stringify(newFriends));
        return postRequest(url, body, API.header());
    }

    static updateFriendProfile(friendProfile) {
        const keys = [
            'contact_id',
            'day',
            'month',
            'hour',
            'minute',
            'year',
            'city_id'
        ];
        const url = `${this.API_ENDPOINT}/contact/update`;
        const body = new FormData();
        lodash.forEach(keys, function(value, key) {
            if (friendProfile.hasOwnProperty(value)) {
                body.append(value, friendProfile[value]);
            }
        });

        return postRequest(url, body, API.header());
    }

    static fetchBase() {
        const url = `${this.API_ENDPOINT}/astro/base`;
        const body = new FormData();
        body.append('test', '');
        return postRequest(url, body, API.header());
    }

    static fetchTimeline() {
        const url = `${this.API_ENDPOINT}/article`;
        return getRequest(url, API.header());
    }
}
