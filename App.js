/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { Provider } from 'react-redux';
import configureStore from './src/redux/configureStore';
import AppStack from './src/router';
import codePush from 'react-native-code-push';

type Props = {};
class App extends Component<Props> {
    render() {
        return (
            <Provider store={configureStore()}>
                <AppStack />
            </Provider>
        );
    }
}

App = codePush(App);

export default App;
