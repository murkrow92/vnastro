import { Platform } from 'react-native';

export const FACEBOOK_APP_ID = '30572648044';
export const API_ENDPOINT = 'http://api.vnastro.com/1.0';
export const APP_LINK_INVITE_ANDROID = 'https://goo.gl/fBj3jr';
export const APP_LINK_INVITE_IOS = 'https://goo.gl/vppnQf';
export const OZI_URL = 'http://ozidigital.com/';
export const VNASTRO_URL = 'http://vnastro.com';
export const BANNER_ID =
    Platform.OS === 'ios'
        ? `ca-app-pub-7892858844183742/4852750903`
        : `ca-app-pub-7892858844183742/4274980103`;
export const INTERSTITIAL_ID = `ca-app-pub-7892858844183742/3615794110`;
export const FANPAGE_ID = '817755621606346';
export const FANPAGE_URL_FOR_BROWSER = `https://fb.com/${FANPAGE_ID}`
export const FANPAGE_URL_FOR_APP = `fb://page/${ FANPAGE_ID}`;
export const WEB_TITLE =  'Học viện Chiêm tinh Việt Nam';

export const GOOGLE_ANALYTIC_TRACKING_ID = 'UA-115187674-1';

export const wizyFacebookID = '100000191705512';

