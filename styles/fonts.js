export const Arial = 'Arial';
export const HelveticaNeueMedium = 'HelveticaNeue-Medium';
export const HelveticaNeueBold = 'HelveticaNeue-Bold';
export const OpenSansRegular = 'OpenSans-Regular';
export const fontello = 'fontello';
export const OpenSansBold = 'OpenSans-Bold';
