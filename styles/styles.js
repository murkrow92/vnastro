import {
    COLOR_DARK,
    COLOR_DARK_33,
    COLOR_GREY_99,
    COLOR_WHITE
} from './colors';

export const container = {
    flex: 1,
    backgroundColor: COLOR_WHITE,
    alignItems: 'flex-start',
    justifyContent: 'flex-start'
};

export const openSans14Dark = {
    fontFamily: 'OpenSans',
    color: COLOR_DARK,
    fontSize: 14
};

export const openSans13Dark33 = {
    fontFamily: 'OpenSans',
    color: COLOR_DARK_33,
    fontSize: 13
};

export const openSans12Grey = {
    fontFamily: 'OpenSans',
    color: COLOR_GREY_99,
    fontSize: 12
};

export const openSans18Dark33 = {
    fontFamily: 'OpenSans',
    color: COLOR_DARK_33,
    fontSize: 18
};

export const containerPadding10 = {
    paddingTop: 10,
    paddingBottom: 10,
    width: '100%',
    flexDirection: 'row'
};

export const area = {
    flex: 1,
    alignItems: 'center'
};

export const arialWhite15 = {
    fontFamily: 'Arial',
    color: COLOR_WHITE,
    fontSize: 15,
};
