export const COLOR_FACEBOOK_BOLD_BLUE = '#3b5998';

export const COLOR_PURPLE = '#7373FA';
export const COLOR_MIDDLE_BLUE = '#9EBEFF';
export const COLOR_LIGHT_BLUE = '#CCEDFF';
export const COLOR_WHITE = '#FFFFFF';
export const COLOR_BLUE = '#007AFF';
export const COLOR_BOLD_GREY = '#999999';
export const COLOR_GREY_AE = '#AEAEAE';
export const COLOR_LIGHT_GREY = '#DFDCD4';
export const COLOR_GREY_F7 = '#F7F7F7';
export const COLOR_GREY_F2 = '#F2F2F2';
export const COLOR_GREY_E0 = '#E0E0E0';
export const COLOR_GREY_BACKGROUND = '#BDC3C7';
export const COLOR_GREY_E4 = '#E4E4E4';
export const COLOR_GREY_D7 = '#D7D7D7';
export const COLOR_DARK_33 = '#333333';
export const COLOR_DARK_66 = '#666666';
export const COLOR_DARK_51 = '#515151';
export const COLOR_GREY_99 = '#999999';
export const COLOR_RIPPLE = '#E9E9E9';
export const COLOR_BLUR = 'rgba(225,225,225, 0.8)';
export const COLOR_GREY_79 = '#797979';

export const COLOR_SIGN_FIRE = '#F05A5B';
export const COLOR_SIGN_WATER = '#23A7EF';
export const COLOR_SIGN_AIR = '#F6D76B';
export const COLOR_SIGN_EARTH = '#87D37B';

export const COLOR_TAB_BAR_BLUE = '#63A3FE';

export const COLOR_DARK = '#000000';